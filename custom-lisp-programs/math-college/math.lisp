(defun factorial (number)
  "Calculates the factorial of a number"
  (cond
    ((= number 1) 1)
    (t (* number
          (factorial (- number 1))))))

(defun variation (n p)
  "Calculates the variaty of two numbers"
  (/ (factorial n)
     (factorial (- n p))))

(defun permutation (number)
  "Calculates the permutation of a number"
  (variaty number number))

(defun repeat-variation (n p)
  "Calculates the repeat-variation of two numbers"
  (expt n p))

(defun combination (n p)
  "Calculates the combination of two numbers"
  (/ (factorial n)
     (* (factorial p)
        (factorial (- n p)))))
