# Practical Common Lisp Exercises

I will go through [this book](http://www.gigamonkeys.com/book),  
and I will do the exercises while doing so.  
I also made some small custom Lisp programs.

# Why?

- To learn Common Lisp
- As an exercise so that I might be able to create the [unnamed-browser-engine](https://gitlab.com/RobinWils/unnamed-browser-engine)

# Progress
- [X] - 1. Introduction: Why Lisp?
- [X] - 2. Lather, Rinse, Repeat: A Tour of the REPL
- [X] - 3. Practical: A Simple Database
- [X] - 4. Syntax and Semantics
- [X] - 5. Functions
- [X] - 6. Variables
- [X] - 7. Macros: Standard Control Constructs
- [ ] - 8. Macros: Defining Your Own
- [ ] - ...
